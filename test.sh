#!/bin/bash
#générateur statique de site web du fablab cohabit
set -o verbose
set -o nounset
LISTE_DES_PROJETS="src="https://projets.cohabit.fr/fablab/tmp/_LISTE_DES_PROJETS"
PAGE_PROJETS_SCIENCE="src="https://projets.cohabit.fr/fablab/projets/fr_science.html"
CORPS_PROJETS_SCIENCES="src="https://projets.cohabit.fr/fablab/tmp/_CORPS_PROJETS_SCIENCES"
CORPSPROJET="src="https://projets.cohabit.fr/fablab/tmp/_CORPS_PROJET"
MENU="src="https://projets.cohabit.fr/fablab/cons/MENU"
ML_RSS="src="https://projets.cohabit.fr/fablab/actus/ML_RSS"
ENTETE="src="https://projets.cohabit.fr/fablab/cons/ENTETE"
PIEDS="src="https://projets.cohabit.fr/fablab/cons/_FOOTER"
INDEX="src="https://projets.cohabit.fr/fablab/index.html"
ACTUSTRIEES="src="https://projets.cohabit.fr/fablab/tmp/actutriees"
CORPS_RSS="src="https://projets.cohabit.fr/fablab/tmp/corps_rss"
ENTETE_RSS="src="https://projets.cohabit.fr/fablab/cons/entete_rss"
PIED_RSS="src="https://projets.cohabit.fr/fablab/cons/pied_rss"
RSS_FEED="src="https://projets.cohabit.fr/fablab/actus/pgp-actus.xml"
HEADER_fr_PROJET="src="https://projets.cohabit.fr/fablab/cons/_HEADER_fr_PROJET"


function checkValues() {
	        grep -w "$1" "$2" | cut -d '|' -f 2
	}
	function checkMedia() {
		        grep -w "$1" "$2" | cut -d '|' -f 1
		}

		# on enregistre l’IFS actuel
		R=$IFS

		# on change l’IFS, pour être un retour à la ligne
		IFS='
		'
		# RAZ des listes tempo
		rm $LISTE_DES_PROJETS
		#rm $CORPS_ACTUS

		### générateur des pages projet fr et en ###

		# générer la liste des Projets
		find /opt/redmine/redmine-3.4.4/files -type f -name "*.projet" | sort -V > $LISTE_DES_PROJETS


		### générateur des pages projet fr et en ###
		cat $LISTE_DES_PROJETS | while read ligne
        	do
		 # Lecture du fichier .projet et enregistrement des variables
		 AUTEUR=`checkValues "_AUTEUR" $ligne`
		 NOMDUPROJET=`checkValues "_NOM_DU_PROJET" $ligne`
		 RESUMEE=`checkValues "_RESUMEE" $ligne`
		 IMAGE=`checkValues "_IMAGE" $ligne`
		 VIDEO=`checkValues "_VIDEO" $ligne`
		 SON=`checkValues "_SON" $ligne`
		 PRESENTATION=`checkValues "_PRESENTATION" $ligne`
		 SOURCES=`checkValues "_SOURCES" $ligne`
		 CATEGORIES=`checkValues "_CATEGORIES" $ligne`
		 DOCUMENTATION=`checkValues "_DOCUMENTATION" $ligne`
		 COMPOSANTS=`checkValues "_COMPOSANTS" $ligne`
		 CATEGORIE=`checkValues "_CATEGORIE" $ligne`
		 # crée une balise html, texte avec lien pour chaque page projet étiquetée sciences
		 # et les enregistre dans le fichier /cons/_INDEX_PROJETS_SCIENCE #
		 #echo -n "<a href=\"src="https://projets.cohabit.fr/fablab/""$NOMDUPROJET"".html\"># "$NOMDUPROJET"" : ""$NOMDUPROJET"</a><br>" >> $INDEX_PROJETS
		 echo -n "$CATEGORIE" >> src="https://projets.cohabit.fr/fablab/test-grep-categories
                done
